package com.example.ghidiem.presentation.features.home.widgets

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ghidiem.R

@Composable
fun UIHomeHeader() {
  Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
      .fillMaxWidth()
      .height(height = 52.dp)
      .background(Color.White)
      .padding(horizontal = 16.dp)
  ) {
    Image(
      painter = painterResource(id = R.drawable.ic_app_logo),
      contentDescription = "",
      modifier = Modifier.size(size = 40.dp)
    )
    Spacer(modifier = Modifier.width(width = 4.dp))
    Text(
      text = "Conversations",
      fontSize = 18.sp,
      fontWeight = FontWeight.W600,
    )
  }
}
