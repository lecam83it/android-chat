package com.example.ghidiem.presentation.common.components

import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun UIText(text: String) {
  Text(text = text)
}