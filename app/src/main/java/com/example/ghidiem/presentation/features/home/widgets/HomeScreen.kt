package com.example.ghidiem.presentation.features.home.widgets

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.ghidiem.R

@Preview
@Composable
fun HomeScreen() {
  Column(
    modifier = Modifier.fillMaxSize(),
    verticalArrangement = Arrangement.Top,
    horizontalAlignment = Alignment.CenterHorizontally,
  ) {
    UIHomeHeader()
    Column(
      modifier = Modifier
        .verticalScroll(
          state = rememberScrollState()
        )
        .fillMaxWidth()
        .background(Color.White)
    ) {
      UIHomeStories()
      UISearchBox()
      UIConversations()
    }
  }
}

@Composable
fun UIConversations() {
  Column {
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
    UIConversationItem()
  }
}

@Composable
fun UIConversationItem() {
  Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
      .padding(horizontal = 20.dp, vertical = 4.dp)
      .height(height = 56.dp)
      .fillMaxWidth()
      .background(Color.White),
  ) {
    Box(
      modifier = Modifier.size(size = 56.dp),
      contentAlignment = Alignment.TopEnd,
    ) {
      AsyncImage(
        model = "https://upload.wikimedia.org/wikipedia/commons/0/06/Tr%C3%BAc_Anh_%E2%80%93_M%E1%BA%AFt_bi%E1%BA%BFc_BTS_%282%29.png",
        contentDescription = null,
        modifier = Modifier
          .offset(x = (-4).dp, y = 4.dp)
          .size(size = 48.dp)
          .clip(shape = RoundedCornerShape(size = 16.dp))
      )
      Box(
        modifier = Modifier
          .size(size = 14.dp)
          .clip(shape = CircleShape)
          .background(color = Color.White),
        contentAlignment = Alignment.Center,
      ) {
        Box(
          modifier = Modifier
            .size(size = 12.dp)
            .clip(shape = CircleShape)
            .background(color = colorResource(id = R.color.success)),
        )
      }
    }
    Spacer(modifier = Modifier.width(width = 12.dp))
    Column {
      Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
      ) {
        Text(
          text = "Fitted - Tech & Design",
          fontSize = 14.sp,
          fontWeight = FontWeight.W600,
          color = colorResource(id = R.color.title),
        )
        Spacer(modifier = Modifier.width(width = 2.dp))
        Text(
          text = "11:20 am",
          fontSize = 10.sp,
          fontWeight = FontWeight.W400,
          color = colorResource(id = R.color.gray_border),
        )
      }
      Spacer(modifier = Modifier.height(height = 2.dp))
      Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()

      ) {
        Box(
          modifier = Modifier
            .weight(weight = 1F, fill = true)
        ) {
          Text(
            text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            fontSize = 11.sp,
            fontWeight = FontWeight.W400,
            color = colorResource(id = R.color.gray_border),
            overflow = TextOverflow.Ellipsis,
            maxLines = 1,
          )
        }
        Spacer(modifier = Modifier.width(width = 2.dp))
        Box(
          modifier = Modifier
            .size(size = 20.dp)
            .clip(shape = CircleShape)
            .background(color = colorResource(id = R.color.red).copy(alpha = 0.12F)),
          contentAlignment = Alignment.Center,
        ) {
          Text(
            text = "1",
            fontSize = 10.sp,
            fontWeight = FontWeight.W600,
            color = colorResource(id = R.color.red),
          )
        }
      }
    }
  }
}

@Composable
fun UISearchBox() {
  Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier
      .padding(horizontal = 20.dp, vertical = 16.dp)
      .height(height = 40.dp)
      .fillMaxWidth()
      .clip(shape = RoundedCornerShape(size = 4.dp))
      .background(color = colorResource(id = R.color.gray_background))
      .padding(all = 8.dp),
  ) {
    Image(
      painter = painterResource(id = R.drawable.ic_baseline_search),
      contentDescription = "",
      modifier = Modifier.size(size = 24.dp)
    )
    Spacer(modifier = Modifier.width(width = 8.dp))
    Text(
      text = "Search...",
      fontSize = 14.sp,
      fontWeight = FontWeight.W600,
      color = colorResource(id = R.color.gray_border),
    )
  }
}


