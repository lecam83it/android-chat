package com.example.ghidiem.presentation.features.home.widgets

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.ghidiem.R

@Composable
fun UIHomeStories() {
  Column {
    Row(
      modifier = Modifier
        .horizontalScroll(
          state = rememberScrollState()
        )
        .fillMaxWidth()
        .background(Color.White)
        .padding(all = 16.dp)
    ) {
      UIAddStoryButton()
      UIStoryItem()
      UIStoryItem()
      UIStoryItem()
      UIStoryItem()
      UIStoryItem()
      UIStoryItem()
      UIStoryItem()
      UIStoryItem()
    }
    Divider()
  }
}

@Composable
fun UIAddStoryButton() {
  Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier.padding(horizontal = 8.dp),
  ) {
    Box(
      modifier = Modifier
        .size(size = 56.dp)
        .border(
          shape = RoundedCornerShape(size = 16.dp),
          width = 2.dp,
          color = colorResource(id = R.color.gray_border)
        )
        .padding(all = 4.dp)
        .clip(shape = RoundedCornerShape(size = 12.dp))
        .background(colorResource(id = R.color.gray_background)),
      contentAlignment = Alignment.Center,
    ) {
      Image(
        painter = painterResource(id = R.drawable.ic_baseline_add),
        contentDescription = "",
        modifier = Modifier.size(size = 24.dp)
      )
    }
    Spacer(modifier = Modifier.height(height = 4.dp))
    Text(
      text = "Your Story",
      fontWeight = FontWeight.Medium,
      fontSize = 10.sp,
    )
  }
}

@Composable
fun UIStoryItem() {
  Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier.padding(horizontal = 8.dp),
  ) {
    Box(
      modifier = Modifier
        .size(size = 56.dp)
        .border(
          shape = RoundedCornerShape(size = 16.dp),
          width = 2.dp,
          color = colorResource(id = R.color.story_border)
        ),
      contentAlignment = Alignment.Center,
    ) {
      AsyncImage(
        model = "https://upload.wikimedia.org/wikipedia/commons/0/06/Tr%C3%BAc_Anh_%E2%80%93_M%E1%BA%AFt_bi%E1%BA%BFc_BTS_%282%29.png",
        contentDescription = null,
        modifier = Modifier
          .size(size = 48.dp)
          .clip(shape = RoundedCornerShape(size = 12.dp))
      )
    }
    Spacer(modifier = Modifier.height(height = 4.dp))
    Box(
      modifier = Modifier
        .width(width = 56.dp),
      contentAlignment = Alignment.Center,
    ) {
      Text(
        text = "Your Story",
        fontWeight = FontWeight.Medium,
        fontSize = 10.sp,
        overflow = TextOverflow.Ellipsis,
        textAlign = TextAlign.Center
      )
    }
  }
}
